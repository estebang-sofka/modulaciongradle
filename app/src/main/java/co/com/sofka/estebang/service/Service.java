package co.com.sofka.estebang.service;

import co.com.sofka.estebang.domain.Persona;
import co.com.sofka.estebang.usecase.PersonaUseCase;

public class Service {

    PersonaUseCase personaUseCase = new PersonaUseCase();

    public Service() {

    }

    public Persona obtenerPersona(String id) {
        try {
            return personaUseCase.obtenerPersona(id);
        } catch (RuntimeException e) {
            return new Persona("0", "No existe esa persona");
        }

    }

}
